<?php

include_once './vendor/autoload.php';

define('AWS_S3_BUCKET_NAME', 'test-sse');
define('AWS_S3_ACCESS_KEY_ID', 'xxx');
define('AWS_S3_SECRET_ACCESS_KEY', 'xxx');
define('AWS_S3_REGION', 'ap-southeast-2');

define('S3_OBJECT_KEY_PREFIX', 'test-ssec-browser');
define('S3_ENCRYPTION_KEY', md5('0')); // 32 characters. Hardcoded to md5 of "0".
define('S3_ENCRYPTION_ALGO', 'AES256');

use Aws\S3\S3Client;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
header("Access-Control-Allow-Headers: *");

/**
 * Client (JS) provide info of the file to start an upload session
 *
 * Server return the presigned urls and required headers to call S3 api
 */
function init_upload()
{
    // $file_name = $_GET['file_name'];
    // $file_content_type = $_GET['file_content_type'];

    //=== Instantiate an Amazon S3 client
    $s3_client = new S3Client([
        'version' => 'latest',
        'region'  => AWS_S3_REGION,
        'credentials' => [
            'key'     => AWS_S3_ACCESS_KEY_ID,
            'secret'  => AWS_S3_SECRET_ACCESS_KEY,
        ]
    ]);

    $s3_object_key = S3_OBJECT_KEY_PREFIX . '-' . time();
    $presigned_url_expiration_time = time() + 3600; // 1h


    //=== Generate presigned url
    try {
        $s3_command_options = $s3_client->getCommand('putObject', [
            'Bucket'               => AWS_S3_BUCKET_NAME,
            'Key'                  => $s3_object_key,
            'SSECustomerKey'       => S3_ENCRYPTION_KEY,
            'SSECustomerKeyMD5'    => md5(S3_ENCRYPTION_KEY, true),
            'SSECustomerAlgorithm' => S3_ENCRYPTION_ALGO,
        ]);

        $presigned_headers = [
            'x-amz-server-side-encryption-customer-algorithm' => S3_ENCRYPTION_ALGO,
            'x-amz-server-side-encryption-customer-key'       => base64_encode(S3_ENCRYPTION_KEY),
            'x-amz-server-side-encryption-customer-key-md5'   => base64_encode(md5(S3_ENCRYPTION_KEY, true)),
        ];

        $pre_signed_url = $s3_client->createPresignedRequest(
            $s3_command_options,
            gmdate(DATE_RFC2822, $presigned_url_expiration_time)
        );

        $res = [
            'key'        => $s3_object_key,
            'url'        => (string) $pre_signed_url->getUri(),
            'headers'    => $presigned_headers,
            'expire_at'  => $presigned_url_expiration_time
        ];
    } catch (Aws\S3\Exception\S3Exception $e) {
        echo "There was an error uploading the file.\n";
    }

    echo json_encode($res);
}

/**
 * The apis entry point
 */
function handle_request()
{
    $request = json_decode(file_get_contents('php://input'), true);
    $action = $request['action'] ?? null;

    switch ($action) {
        case 'init_upload':
            init_upload();
            break;

        default:
            header('Content-Type: application/json');
            echo json_encode([
                'message' => 'Unknown action to process'
            ]);
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    handle_request();
}
