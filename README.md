### Run PHP API server
```
cd server
composer install
php -S localhost:9000
```

### Run Vue app
```
cd app
yarn install
yarn serve
```